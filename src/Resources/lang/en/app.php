<?php
return [
    
    
    'category' => [
        'title'                 => 'Category',

    ],


    'categories' => [
        'type'                 => 'Type',
        'select_type'          => 'Select Type',

    ],
    
    'category_type' => [
        'title'                 => 'Category Type',
        
        'add-type-btn-title'    => 'Add Category Type',
        'add-title'             => 'Add Category Type',
        'save-btn-title'        => 'Save',
        'edit-title'            => 'Edit Category Type',
        'update-btn-title'      => 'Update',


        'general'               => 'General',
        'status_type'           => 'Status & Type',
        'code'                  => 'Code',
        'name'                  => 'Name',
        'status'                => 'Status',
        'status-yes'            => 'Yes',
        'status-no'             => 'No',

    ],


    'datagrid' => [
        'id'                    => 'ID',
        'code'                  => 'Code',
        'name'                  => 'Category\'s Name',
        'email'                 => 'Email',
        'mobile'                => 'Mobile',
        'type'                  => 'Type',
        'status'                => 'Status',
        'is_verified'           => 'Verified',
        'delete'                => 'Do you really want to Delete?',
    ],

    'response' => [
        'create-success'        => 'Category Added Successfully',
        'create-failed'         => 'Category Added Failed',
        'update-success'        => 'Category Update Successfully',
        'update-failed'         => 'Category Update Failed',
        'delete-success'        => 'Category Deleted Successfully',
        'delete-failed'         => 'Category Delete Failed',


        'type-create-success'   => 'Category Type Added Successfully',
        'type-create-failed'    => 'Category Type Added Failed',
        'type-update-success'   => 'Category Type Update Successfully',
        'type-update-failed'    => 'Category Type Update Failed',
        'type-delete-success'   => 'Category Type Deleted Successfully',
        'type-delete-failed'    => 'Category Type Delete Failed',
    ],
    'acl' => [
        'category-type'         => 'Category Type'
    ]
];